package eu.mokari.togo;

import com.facebook.stetho.Stetho;

public class ToGoDebugApplication extends ToGoApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
