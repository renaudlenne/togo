package eu.mokari.togo;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.SphericalUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import eu.mokari.togo.adapters.PlaceAutocompleteAdapter;
import eu.mokari.togo.api.GoogleMapsService;
import eu.mokari.togo.api.GooglePlaceDetails;
import eu.mokari.togo.api.OpeningHours;
import eu.mokari.togo.api.Period;
import eu.mokari.togo.api.Result;
import eu.mokari.togo.models.OpeningPeriod;
import eu.mokari.togo.models.Place;
import eu.mokari.togo.models.PlaceType;
import eu.mokari.togo.services.GeofenceTransitionsIntentService;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends AppCompatActivity implements
        GoogleMap.OnMyLocationButtonClickListener,
        OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnInfoWindowClickListener {

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final String TAG = MapsActivity.class.getSimpleName();

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    private GoogleMap mMap;

    private MapsActivity thisActivity = this;
    private GoogleApiClient client;
    private Map<String, Place> places = new HashMap<>();
    private Map<String, Marker> markers = new HashMap<>();
    private PendingIntent mGeofencePendingIntent;
    private List<Geofence> mGeofenceList;
    private LocationManager locationmanager;

    private GoogleMapsService service = new Retrofit.Builder()
            .baseUrl("https://maps.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GoogleMapsService.class);
    private DatabaseReference mDatabase;
    private DatabaseReference mUserDb;

    private PlaceAutocompleteAdapter mAdapter;
    private GeoDataClient mGeoDataClient;

    private void initializeFirebaseListener() {
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());

                // A new place has been added, add it to the displayed list
                Place place = dataSnapshot.getValue(Place.class);
                place.id = dataSnapshot.getKey();
                places.put(place.id, place);

                if (mMap != null) {
                    Marker marker = place.addToMap(mMap);
                    markers.put(marker.getId(), marker);
                }
                addGeofence(place);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());

                // A place has changed, use the key to determine if we are displaying this
                // place and if so displayed the changed place.
                Place newPlace = dataSnapshot.getValue(Place.class);
                newPlace.id = dataSnapshot.getKey();
                Place prevPlace = places.get(newPlace.id);
                if(prevPlace.markerId != null) {
                    newPlace.markerId = prevPlace.markerId;
                } else if (mMap != null) {
                    Marker marker = newPlace.addToMap(mMap);
                    markers.put(marker.getId(), marker);
                }
                places.put(newPlace.id, newPlace);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

                // A place has changed, use the key to determine if we are displaying this
                // place and if so remove it.
                String placeKey = dataSnapshot.getKey();
                Place place = places.remove(placeKey);

                if(place.markerId != null) {
                    markers.get(place.markerId).remove();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());
                // Nothing to do here, we don't display an ordered list
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "postPlaces:onCancelled", databaseError.toException());
                Toast.makeText(MapsActivity.this, "Failed to load places.", Toast.LENGTH_SHORT).show();
            }
        };
        fbPlaces().addChildEventListener(childEventListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        client = new GoogleApiClient.Builder(thisActivity)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .enableAutoManage(this, this)
                .build();

        locationmanager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mDatabase = ToGoApplication.firebaseDb.getReference();
        mUserDb = mDatabase.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mUserDb.keepSynced(true);
        initializeFirebaseListener();

        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(this);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data Client.
        mAdapter = new PlaceAutocompleteAdapter(this, mGeoDataClient, null, null);

        FloatingActionButton plusButton = (FloatingActionButton) findViewById(R.id.plus_button);
        assert plusButton != null;
        plusButton.setOnClickListener(new View.OnClickListener() {

            protected View findSelectedView(Collection<View> views) {
                for (View v : views) {
                    if (v.isSelected())
                        return v;
                }
                return null;
            }

            @Override
            public void onClick(View v) {

                final Map<View, PlaceType> choiceButtons = new HashMap<>();

                LayoutInflater li = LayoutInflater.from(MapsActivity.this);
                View dialogView = li.inflate(R.layout.place_creation_dialog, null);
                final AutoCompleteTextView nameView = (AutoCompleteTextView) dialogView.findViewById(R.id.name);
                final EditText addressView = (EditText) dialogView.findViewById(R.id.address);
                nameView.setAdapter(mAdapter);
                nameView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        AutocompletePrediction prediction = mAdapter.getItem(i);
                        CharSequence newAddress = prediction.getSecondaryText(null);
                        Log.d(TAG, "Change address to:"+newAddress);
                        addressView.setText(newAddress);
                    }
                });

                final MaterialDialog dialog = new MaterialDialog.Builder(thisActivity)
                        .title(R.string.place_creation_dialog_title)
                        .customView(dialogView, true)
                        .positiveText("Create")
                        .negativeText("Cancel")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                View selectedView = findSelectedView(choiceButtons.keySet());
                                PlaceType placeType = choiceButtons.get(selectedView);
                                newLocation(nameView.getText().toString(), addressView.getText().toString(), placeType);
                            }
                        }).show();

                dialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                choiceButtons.put(dialog.getCustomView().findViewById(R.id.choice_restaurant), PlaceType.RESTAURANT);
                choiceButtons.put(dialog.getCustomView().findViewById(R.id.choice_museum), PlaceType.MUSEUM);
                choiceButtons.put(dialog.getCustomView().findViewById(R.id.choice_nature), PlaceType.NATURE);
                choiceButtons.put(dialog.getCustomView().findViewById(R.id.choice_shopping), PlaceType.SHOPPING);

                for (View button : choiceButtons.keySet()) {
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
                            View selectedView = findSelectedView(choiceButtons.keySet());
                            if (selectedView != null)
                                selectedView.setSelected(false);
                            v.setSelected(true);
                        }
                    });
                }
            }
        });
    }

    public LatLngBounds toBounds(LatLng center, double radius) {
        LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
        LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 45);
        return new LatLngBounds(southwest, northeast);
    }

    private DatabaseReference fbPlaces() {
        return mUserDb.child("places");
    }

    private void newLocation(String name, String address, PlaceType placeType) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            Address location = geocoder.getFromLocationName(address, 1).get(0);
            Place place = Place.fromAddressNameAndType(location, name, placeType);

            findGooglePlaceInfo(place);
            DatabaseReference fbRef = fbPlaces().push();
            place.id = fbRef.getKey();
            fbRef.setValue(place);
            if(mMap != null)
                centerMapOnLocation(place.getLatLng());
        } catch (IOException | IndexOutOfBoundsException e) {
            Toast.makeText(this, "Cannot find this address :(", Toast.LENGTH_SHORT).show();
        }
    }

    protected void centerMapOnLocation(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        centerMapOnLocation(latLng);
    }

    protected void centerMapOnLocation(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.0f));
    }

    private void addGeofence(Place place) {
        if(client.isConnected()) {
            Geofence geofence = new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(place.id)

                    .setCircularRegion(
                            place.latitude,
                            place.longitude,
                            Constants.GEOFENCE_RADIUS_IN_METERS
                    )
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .build();


            GeofencingRequest request = new GeofencingRequest.Builder()
                    .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    .addGeofence(geofence).build();

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission to access the location is missing.
                PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                        Manifest.permission.ACCESS_FINE_LOCATION, true);
                return;
            }
            LocationServices.GeofencingApi.addGeofences(client, request, getGeofencePendingIntent());
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void findGooglePlaceInfo(final Place place) {
        LatLngBounds bounds = toBounds(place.getLatLng(), 1000);
        Places.GeoDataApi.getAutocompletePredictions(client, place.name, bounds, null).setResultCallback(new ResultCallback<AutocompletePredictionBuffer>() {
            @Override
            public void onResult(@NonNull AutocompletePredictionBuffer autocompletePredictions) {
                if (autocompletePredictions.iterator().hasNext()) {
                    place.googlePlaceId = autocompletePredictions.get(0).getPlaceId();
                    fbPlaces().child(place.id).setValue(place);
                    try {
                        ExecutorService pool = Executors.newSingleThreadExecutor();
                        Result result = pool.submit(new Callable<Result>() {
                            @Override
                            public Result call() throws Exception {
                                try {
                                    Response<GooglePlaceDetails> response = service.placeDetails(place.googlePlaceId, getString(R.string.web_gmap_api_key)).execute();
                                    if(response.isSuccessful()) {
                                        Result result = response.body().getResult();
                                        if(result == null) {
                                            Log.d(TAG, "Error response from Places API: " + response.body().getErrorMessage());
                                        }
                                        return result;
                                    } else {
                                        Log.d(TAG, "Error "+response.code()+" calling Places API: "+response.message() + " - "+response.errorBody().string());
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        }).get();
                        if (result != null) {
                            OpeningHours openingHours = result.getOpeningHours();
                            place.openingPeriods = new ArrayList<>();
                            for (Period period : openingHours.getPeriods()) {
                                OpeningPeriod openingPeriod = new OpeningPeriod();
                                openingPeriod.dayOfWeek = period.getOpen().getDay();
                                openingPeriod.openTime = Integer.parseInt(period.getOpen().getTime());
                                openingPeriod.closeTime = Integer.parseInt(period.getClose().getTime());
                                place.openingPeriods.add(openingPeriod);
                            }
                            fbPlaces().child(place.id).setValue(place);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        for (Place place : places.values()) {
            Marker marker = place.addToMap(mMap);
            markers.put(marker.getId(), marker);
        }

        mMap.setOnMapLongClickListener(this);
        mMap.setOnInfoWindowClickListener(this);

        mMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);

            Criteria cri = new Criteria();
            String bbb = locationmanager.getBestProvider(cri, true);
            Location lastKnownLocation = locationmanager.getLastKnownLocation(bbb);

            if(lastKnownLocation != null)
                centerMapOnLocation(lastKnownLocation);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //TODO handle connection failed (or not ?)
    }

    protected Place placeForMarker(Marker marker) {
        for(Place place: places.values()) {
            if(place.markerId.equals(marker.getId()))
                return place;
        }
        throw new IllegalArgumentException("No place for this marker.");
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //TODO create a new place from this point
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Place place = placeForMarker(marker);
        editPlace(place);
    }

    private void editPlace(Place place) {
        //TODO show the edition dialog/window for this place
        findGooglePlaceInfo(place);
    }
}
