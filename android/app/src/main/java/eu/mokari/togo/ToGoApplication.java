package eu.mokari.togo;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

public class ToGoApplication extends Application {

    static public FirebaseDatabase firebaseDb;

    @Override
    public void onCreate() {
        super.onCreate();
        firebaseDb = FirebaseDatabase.getInstance();
        firebaseDb.setPersistenceEnabled(true);
    }
}