package eu.mokari.togo.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapsService {

    @GET("/maps/api/place/details/json")
    Call<GooglePlaceDetails> placeDetails(@Query("placeid") String placeId, @Query("key") String apiKey);

}
