
package eu.mokari.togo.api;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DebugLog {

    @SerializedName("line")
    @Expose
    private List<Object> line = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The line
     */
    public List<Object> getLine() {
        return line;
    }

    /**
     * 
     * @param line
     *     The line
     */
    public void setLine(List<Object> line) {
        this.line = line;
    }

}
