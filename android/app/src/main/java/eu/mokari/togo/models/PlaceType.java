package eu.mokari.togo.models;

public enum PlaceType {
    RESTAURANT, SHOPPING, MUSEUM, NATURE
}
