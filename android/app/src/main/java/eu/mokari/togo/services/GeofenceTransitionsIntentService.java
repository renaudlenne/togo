package eu.mokari.togo.services;

import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import eu.mokari.togo.R;

public class GeofenceTransitionsIntentService extends IntentService {

    private static final String TAG = GeofenceTransitionsIntentService.class.getSimpleName();

    public GeofenceTransitionsIntentService() {
        super(TAG);
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }

    private int getErrorStringResId(int errorCode) {
        switch (errorCode) {
            case GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE:
                return R.string.unknown_geofence_error;
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES:
                return R.string.geofence_too_many_geofences;
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS:
                return R.string.geofence_too_many_pending_intents;
            default:
                return R.string.unknown_geofence_error;
        }
    }

    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Toast.makeText(this, getErrorStringResId(geofencingEvent.getErrorCode()), Toast.LENGTH_LONG).show();
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Send notification and log the transition details.
            sendNotification(triggeringGeofences);
        }
    }

        /**
         * Posts a notification in the notification bar when a transition is detected.
         * If the user clicks the notification, control goes to the MainActivity.
         */
        private void sendNotification(List<Geofence> triggeringGeofences) {
            GeofenceEnteredNotification.notify(this, triggeringGeofences);

//            // Create an explicit content Intent that starts the main Activity.
//            Intent notificationIntent = new Intent(getApplicationContext(), MapsActivity.class);
//
//            // Construct a task stack.
//            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//
//            // Add the main Activity to the task stack as the parent.
//            stackBuilder.addParentStack(MapsActivity.class);
//
//            // Push the content Intent onto the stack.
//            stackBuilder.addNextIntent(notificationIntent);
//
//            // Get a PendingIntent containing the entire back stack.
//            PendingIntent notificationPendingIntent =
//                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//            // Get a notification builder that's compatible with platform versions >= 4
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//
//            String content;
//            if(triggeringGeofences.size() > 1) {
//                content = "Il y a des trucs sympa à voir dans le coin !";
//            } else {
//                int placeId = Integer.parseInt(triggeringGeofences.get(0).getRequestId());
//                Place place = PlaceService.get(placeId);
//                content = "Vous vous approchez de "+place.name+", ouvrez l'oeil!";
//            }
//
//            // Define the notification settings.
//            builder.setSmallIcon(R.mipmap.ic_launcher)
//                    // In a real app, you may want to use a library like Volley
//                    // to decode the Bitmap.
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
//                            R.mipmap.ic_launcher))
//                    .setColor(Color.RED)
//                    .setContentTitle("Near a place to go!")
//                    .setContentText(content)
//                    .setContentIntent(notificationPendingIntent);
//
//            // Dismiss notification once the user touches it.
//            builder.setAutoCancel(true);
//
//            // Get an instance of the Notification manager
//            NotificationManager mNotificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            // Issue the notification
//            mNotificationManager.notify(0, builder.build());
        }
}