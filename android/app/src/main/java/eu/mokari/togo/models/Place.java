package eu.mokari.togo.models;

import android.location.Address;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.Exclude;

import java.util.List;

import eu.mokari.togo.R;

public class Place {

    public String name;

    public String address;

    @Exclude
    public PlaceType placeType;

    @Exclude
    public String id;

    public String getPlaceType() {
        // Convert enum to string
        if (placeType == null) {
            return null;
        } else {
            return placeType.name();
        }
    }

    public void setPlaceType(String placeTypeStr) {
        // Get enum from string
        if (placeTypeStr == null) {
            this.placeType = null;
        } else {
            this.placeType = PlaceType.valueOf(placeTypeStr);
        }
    }

    public double latitude;

    public double longitude;

    public String googlePlaceId;

    public List<OpeningPeriod> openingPeriods;

    public List<Tag> tags;

    public static Place fromAddressNameAndType(Address address, String name, PlaceType placeType) {
        Place place = new Place();
        place.name = name;
        place.address = address.getAddressLine(0);
        place.latitude = address.getLatitude();
        place.longitude = address.getLongitude();
        place.placeType = placeType;
        return place;
    }

    @Exclude
    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }

    protected int markerResourceId() {
        switch (placeType) {
            case MUSEUM:
                return R.drawable.museum_marker_selected;
            case NATURE:
                return R.drawable.nature_marker_selected;
            case RESTAURANT:
                return R.drawable.restaurant_marker_selected;
            case SHOPPING:
                return R.drawable.shopping_marker_selected;
        }
        throw new IllegalArgumentException("Unknown place type");
    }

    @Exclude
    public String markerId;
    public Marker addToMap(GoogleMap map) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(getLatLng())
                .title(name)
                .icon(BitmapDescriptorFactory.fromResource(markerResourceId()));
        Marker marker = map.addMarker(markerOptions);
        markerId = marker.getId();
        return marker;
    }

}
